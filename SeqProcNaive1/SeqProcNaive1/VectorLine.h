#pragma once
class VectorLine
{
public:
	float m, c; // For y=mx+c
	float a, b, cp; // For ax+by+c=0
	// int direction; // +ve x_or_y, or -ve x_or_y
	float dx, dy; // For unit vector (dx,dy)

	VectorLine(float m, float c, float dx, float dy);
	VectorLine(float a, float b, float cp, float dx, float dy);
	VectorLine();
	~VectorLine();
};