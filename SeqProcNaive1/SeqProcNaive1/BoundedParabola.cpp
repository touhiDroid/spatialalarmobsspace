#include "BoundedParabola.h"
#include <math.h>



BoundedParabola::BoundedParabola(float focusX, float focusY, 
	float aDirtx, float bDirtx, float cDirtx, 
	float aBoundLine, float bBoundLine, float cBoundLine)
{
	this->focusX = focusX;
	this->focusY = focusY;
	this->aDirtx = aDirtx;
	this->bDirtx = bDirtx;
	this->cDirtx = cDirtx;
	this->aBoundLine = aBoundLine;
	this->bBoundLine = bBoundLine;
	this->cBoundLine = cBoundLine;

	// Calculate eq. of the parabola as : g(x,y) = Ax^2 + Bxy + Cy^2 + Dx + Ey + F
	float a2 = aDirtx*aDirtx;
	float b2 = bDirtx*bDirtx;
	this->A = b2;
	this->B = -2 * aDirtx*bDirtx;
	this->C = a2;
	this->D = -2 * focusX  * (a2+b2) - 2 * cDirtx*aDirtx;
	this->E = -2 * focusY * (a2+b2) - 2 * bDirtx*cDirtx;
	this->F = (focusX*focusX + focusY*focusY) * (a2+b2) - cDirtx*cDirtx;

	float md = -(aDirtx / bDirtx);
	float cd = -(cDirtx / bDirtx);
	float ma = -1.0 / md;
	float ca = focusY - ma * focusX;
	float dax = (ca - cd) / (ma - md);
	float day = (md*ca - ma*cd) / (ma - md);
	float dx = focusX - dax;
	float dy = focusY - day;
	float dxy = sqrt(dx*dx - dy*dy);
	dx /= dxy;
	dy /= dxy;
	this->sAxis = VectorLine(ma, ca, dx, dy);
	this->sDirtx = VectorLine(md, cd, dy, -dx);
}
BoundedParabola::BoundedParabola(float focusX, float focusY,
	float aDirtx, float bDirtx, float cDirtx,
	float aBoundLine, float bBoundLine, float cBoundLine, VectorLine sAxis)
{
	this->focusX = focusX;
	this->focusY = focusY;
	this->aDirtx = aDirtx;
	this->bDirtx = bDirtx;
	this->cDirtx = cDirtx;
	this->aBoundLine = aBoundLine;
	this->bBoundLine = bBoundLine;
	this->cBoundLine = cBoundLine;

	// Calculate eq. of the parabola as : g(x,y) = Ax^2 + Bxy + Cy^2 + Dx + Ey + F
	float a2 = aDirtx*aDirtx;
	float b2 = bDirtx*bDirtx;
	this->A = b2;
	this->B = -2 * aDirtx*bDirtx;
	this->C = a2;
	this->D = -2 * focusX  * (a2 + b2) - 2 * cDirtx*aDirtx;
	this->E = -2 * focusY * (a2 + b2) - 2 * bDirtx*cDirtx;
	this->F = (focusX*focusX + focusY*focusY) * (a2 + b2) - cDirtx*cDirtx;

	float md = -(aDirtx / bDirtx);
	float cd = -(cDirtx / bDirtx);
	float ma = -1.0 / md;
	float ca = focusY - ma * focusX;
	float dax = (ca - cd) / (ma - md);
	float day = (md*ca - ma*cd) / (ma - md);
	float dx = focusX - dax;
	float dy = focusY - day;
	float dxy = sqrt(dx*dx - dy*dy);
	dx /= dxy;
	dy /= dxy;
	this->sAxis = sAxis;
	this->sDirtx = VectorLine(md, cd, dy, -dx);
}

BoundedParabola::BoundedParabola(float * focus, VectorLine sAxis, float mpr, float npr)
{
	this->focusX = focus[0];
	this->focusY = focus[1];
	float* u = getVertex(sAxis, mpr);

	float mDirtx = -1.0 / sAxis.m;
	float cDirtx = (2 * u[1] - focus[1]) 
		- mDirtx * (2 * u[0] - focus[0]);

	float mBnd = mDirtx;
	float bx = focus[0] + (npr) * sAxis.dx;
	float by = focus[1] + (npr) * sAxis.dy;
	float cBnd = by - mBnd*bx;

	*this = BoundedParabola(focus[0], focus[1],
		mDirtx, -1.0, cDirtx,
		mBnd, -1.0, cBnd, sAxis);
}

BoundedParabola::BoundedParabola()
{
}


BoundedParabola::~BoundedParabola()
{
}

float BoundedParabola::getPointval(float x, float y)
{
	// Calculate & return : g(x,y) = Ax^2 + Bxy + Cy^2 + Dx + Ey + F
	return (A*x*x + B*x*y + C*y*y + D*x + E*y + F);
}

float BoundedParabola::getDeterminantVal()
{
	return (
		  2 * A * (4 * C*F - E*E) 
		- B * (2 * B*F - E*D) 
		+ D * (B*E - 2 * C*D)
	);
}

bool BoundedParabola::isPointInside(float x, float y)
{
	// Find whether on the same side of the bounding st. line as the focus
	float fVal = aBoundLine*focusX + bBoundLine*focusY + cBoundLine;
	float pVal = aBoundLine*x + bBoundLine*y + cBoundLine;
	if ((fVal > 0 && pVal < 0) || (fVal < 0 && pVal > 0) || pVal != 0)
		return false;

	// Find whether inside the parabollic section
	float gVal = getPointval(x, y);
	float detVal = getDeterminantVal();

	if ((gVal < 0 && detVal < 0) || (gVal < 0 && detVal < 0) || gVal == 0)
		return true;
	return false;
}

bool BoundedParabola::isPointInsideParabola(float x, float y)
{
	// Find whether inside the parabollic section
	float gVal = getPointval(x, y);
	float detVal = getDeterminantVal();

	if ((gVal < 0 && detVal < 0) || (gVal < 0 && detVal < 0) || gVal == 0)
		return true;
	return false;
}
