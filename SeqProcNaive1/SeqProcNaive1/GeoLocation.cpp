#include "GeoLocation.h"
#include "Utils/AppConstants.h"
#include <stdio.h>

#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS

GeoLocation::GeoLocation(float longitude, float lattitude, float altitude)
{
	this->longitude = longitude;
	this->lattitude = lattitude;
	if (altitude < AppConstants::MIN_ALTITUDE)
		altitude = AppConstants::MIN_ALTITUDE;
	this->altitude = altitude;
}
GeoLocation::GeoLocation(float longitude, float lattitude)
{
	this->longitude = longitude;
	this->lattitude = lattitude;
	this->altitude = AppConstants::MIN_ALTITUDE;
}


GeoLocation::GeoLocation()
{
	this->longitude = 0;
	this->lattitude = 0;
	this->altitude = AppConstants::MIN_ALTITUDE;
}


GeoLocation::~GeoLocation()
{
}

char* GeoLocation::getLocationStr()
{
	char* s = new char[40];
	printf("(Longi=%3.2lf, Lati=%3.2lf, Alti=%3.2lf)", longitude, lattitude, altitude);
	sprintf_s(s, sizeof(s), "(Longi=%3.2lf, Lati=%3.2lf, Alti=%3.2lf)",
		longitude, lattitude, altitude);
	return s;
}