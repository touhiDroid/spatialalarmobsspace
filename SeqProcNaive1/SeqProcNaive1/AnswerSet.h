#pragma once
#include "GeoLocation.h"
#include <vector>
#include "visGraph/obstacles.h"
#include "visGraph/VisibilityGraph.h"
#include "ognn_utility.h"
#include "BoundedParabola.h"
using namespace std;

class AnswerSet
{
public:
	GeoLocation location;
	int minDistToUpdate;
	vector<Obstacle*> poiSet;
	//vector<Obstacle*> obsSet;
	VisibilityGraph* visGraph;
	vector<MyStruct> distO_vect;

	BoundedParabola poiParabola, obsParabola;

	AnswerSet() {}

	AnswerSet(GeoLocation location, int minDistToUpdate,
		VisibilityGraph* visGraph);
	
	AnswerSet(GeoLocation location, int minDistToUpdate,
		vector<Obstacle*> poiSet, //vector<Obstacle*> obsSet,
		VisibilityGraph* visGraph);

	AnswerSet(GeoLocation location, vector<Obstacle*> poiSet,
		VisibilityGraph* visGraph,
		//vector<Obstacle*> obsSet, 
		vector<MyStruct> distO_q_pi);

	AnswerSet(GeoLocation location,
		BoundedParabola piPoi, BoundedParabola piObst,
		vector<Obstacle*> poiSet,
		VisibilityGraph* visGraph,
		//vector<Obstacle*> obsSet, 
		vector<MyStruct> distO_q_pi);

	~AnswerSet();
};

