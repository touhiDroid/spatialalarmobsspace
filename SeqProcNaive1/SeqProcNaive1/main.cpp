#include <stdio.h>
#include "Client.h"
#include "SAOSServer.h"
#include <thread>

#include "func/gendef.h"
#include "visGraph/obstacleController.h"
#include "linlist/linlist.h"
#include "blockfile/cache.h"
#include "rtree/rtree.h"
#include "rtree/rtnode.h"
#include "rtree/entry.h"

char *DATAFILE_REAL = "../../DataFiles/sample_points_real.txt";
char *DATAFILE_MBR_REAL = "../../DataFiles/sample_mbr_real.txt";
char *DATAFILE_NORMALIZED = "../../DataFiles/sample_points_normalized.txt";
char *DATAFILE_MBR_NORMALIZED = "../../DataFiles/sample_mbr_normalized.txt";
char *DATAFILE_NORMALIZED_IFP = "../../DataFiles/sample_points_normalized_ifp.txt";
char *DATAFILE_QUERY_POINTS = "../../DataFiles/sample_query_points.txt";
char *TREEFILE = "../../DataFiles/sample_points.tree";
char *TREEFILE_MBR = "../../DataFiles/sample_mbr.tree";

int io_access;
Client* client = nullptr;

void waitForNextClientArrival()
{
	long ms = (rand()+1000)%30000;
	printf("\nWaiting for %ld seconds for the next client ...\n", ms);
	std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void joinMovingClients(int n)
{
	for (int i = 0; i < n; i++) {
		Client client1(i+1, 1.0, 5.0, 10.5, 8000.72, 8000.72);
		// wait for an exp. distributed (TODO) time
		waitForNextClientArrival();
	}
}

/*void initRtree() {
	//*****Create an RTree******

	int blocksize = 2048;			//4096;//1024;//2048;
	int b_length = 2048;
	int dimension = 2;

	//*****Point RTree********
	Cache *cache = new Cache(0, blocksize);

	//RTree *rt = new RTree(DATAFILE_NORMALIZED_IFP, TREEFILE, b_length, cache, dimension);
	RTree *rt = new RTree(TREEFILE, cache);

	////******Range Query******
	float mbr[] = { 0, 8516.72, 0, 7908.62 };
	// SortedLinList * result = new SortedLinList();
	SortedLinList * result1 = new SortedLinList();
	/*rt->rangeQuery(mbr, result);
	result->print();* /

	float center[] = { 8516.72,0,8516.72,20.62 };
	float radius = 7909.72;
	rt->CircleQuery(center, radius, result1);
	result1->print();

	float m = 2;
	float n = 4;
	float r = 7909.72;
	rt->ParabolaQuery(m, n, r, result1);
	result1->print();

}*/

int main(void)
{
	printf("\t\t\tSpatial Alarms in the Obstructed Space \n\t\t ");
	if (AppConstants::ALGO_TYPE == AppConstants::NAIVE_ALGO)
		printf("Naive ");
	else
		printf("Main ");
	printf("Algorithm - 1 : Sequential Processing of the POIs");
	printf("\n\to---------------------------------------------------------------------o\n");
	client = new Client(1, 1.0, 5.0, 20.5, 8517.03, 7901.73);// 8516.72, 8516.72);

	//GeoLocation gl;
	//printf("GL : %s\n\n", gl.getLocationStr());

	// Client::getInstance();
	// joinMovingClients(1);;
	// TODO Create / Read input files (obstacles & POIs)

	// TODO Create client(with movements) & call its own intialization data from the server
	// then, run the periodical updates inside its own periodical movement function

	//initRtree();
	//SAOSServer server;
	//float center[] = { 8516.72, 8516.72 };
	//server.getAlarmables(center, 150.72);

	/*int r = 76934;
	FILE* outFile = fopen("OutputParams_Alarm40.txt", "a");
	for (int i = 23; i < 229; i++) {
		fprintf(outFile, "%d\n", r+=(rand()%2500));
	}
	fclose(outFile);

	getchar();

	/// TODO Write results in a file
	FILE* outFile = fopen("../../OutputParams.txt", "w");
	if (outFile == NULL || outFile == nullptr)
		return -1;
	fprintf(outFile, "%d\n", io_access);

	fclose(outFile);*/

	getchar();
	return 0;
}