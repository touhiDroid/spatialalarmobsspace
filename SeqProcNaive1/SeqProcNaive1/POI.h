#pragma once
#include <string>
#include "GeoLocation.h"

class POI
{

private:
	int poiId;
	GeoLocation poiLoc;
	int alarmRadius, typeOfAlarm;
	std::string poiName, alarmMessage;


public:
	enum { PRIVATE_ALARM, SHARED_ALARM, PUBLIC_ALARM };

	POI();
	POI(int poiId, float longitude, float lattitude, float altitude,
		int alarmRadius, int typeOfAlarm, std::string poiName, std::string alarmMessage);
	POI(int poiId, float longitude, float lattitude,
		int alarmRadius, int typeOfAlarm, std::string poiName, std::string alarmMessage);
	POI(int poiId, GeoLocation poiLoc, int alarmRadius, 
		int typeOfAlarm, std::string poiName, std::string alarmMessage);
	~POI();

	int get_poi_id();
	GeoLocation get_poi_location();
	int get_alarm_radius();
	int get_type_of_alarm();
	std::string get_poi_name();
	std::string get_alarm_message();

	void set_poi_id(int poiId);
	void set_poi_location(GeoLocation poiLoc);
	void set_alarm_radius(int alarmRadius);
	void set_type_of_alarm(int typeOfAlarm);
	void set_poi_name(std::string poiName);
	void set_alarm_message(std::string alarmMessage);
};

