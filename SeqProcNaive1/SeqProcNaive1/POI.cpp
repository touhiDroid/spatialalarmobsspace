#include "POI.h"

POI::POI(int poiId, float longitude, float lattitude, float altitude,
	int alarmRadius, int typeOfAlarm, std::string poiName, std::string alarmMessage)
{
	this->poiId = poiId;
	this->poiLoc = GeoLocation(longitude, lattitude, altitude);
	this->alarmRadius = alarmRadius; // radius is in meter (m)
	this->typeOfAlarm = typeOfAlarm;
	this->poiName = poiName;
	this->alarmMessage = alarmMessage;
}

POI::POI(int poiId, float longitude, float lattitude, int alarmRadius,
	int typeOfAlarm, std::string poiName, std::string alarmMessage)
{
	this->poiId = poiId;
	this->poiLoc = GeoLocation(longitude, lattitude);
	this->alarmRadius = alarmRadius; // radius is in meter (m)
	this->typeOfAlarm = typeOfAlarm;
	this->poiName = poiName;
	this->alarmMessage = alarmMessage;
}

POI::POI(int poiId, GeoLocation poiLoc, int alarmRadius,
	int typeOfAlarm, std::string poiName, std::string alarmMessage)
{
	this->poiId = poiId;
	this->poiLoc = poiLoc;
	this->alarmRadius = alarmRadius; // radius is in meter (m)
	this->typeOfAlarm = typeOfAlarm;
	this->poiName = poiName;
	this->alarmMessage = alarmMessage;
}


POI::POI()
{
	this->poiId = -1;
	this->poiLoc = GeoLocation();
	this->alarmRadius = 100;
	this->typeOfAlarm = POI::PRIVATE_ALARM;
	this->poiName = "POI";
	this->alarmMessage = "Message to the client when this POI is reached.";
}

POI::~POI()
{
	this->poiName = "";
	this->alarmMessage = "";
}

// Getter methods
int POI::get_poi_id()
{
	return this->poiId;
}
GeoLocation POI::get_poi_location()
{
	return this->poiLoc;
}
int POI::get_alarm_radius()
{
	return this->alarmRadius;
}
int POI::get_type_of_alarm()
{
	return this->typeOfAlarm;
}
std::string POI::get_poi_name()
{
	return this->poiName;
}
std::string POI::get_alarm_message()
{
	return this->alarmMessage;
}

// Setter methods
void POI::set_poi_id(int poiId)
{
	this->poiId = poiId;
}
void POI::set_poi_location(GeoLocation poiLoc)
{
	this->poiLoc = poiLoc;
}
void POI::set_alarm_radius(int alarmRadius)
{
	this->alarmRadius = alarmRadius;
}
void POI::set_type_of_alarm(int typeOfAlarm)
{
	this->typeOfAlarm = typeOfAlarm;
}
void POI::set_poi_name(std::string poiName)
{
	this->poiName = poiName;
}
void POI::set_alarm_message(std::string alarmMessage)
{
	this->alarmMessage = alarmMessage;
}

// Other utility methods : Currently not needed
