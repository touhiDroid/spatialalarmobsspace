#include "VectorLine.h"
#pragma once
class BoundedParabola
{
public:
	float A, B, C, D, E, F;
	float focusX, focusY;
	float aDirtx, bDirtx, cDirtx;
	float aBoundLine, bBoundLine, cBoundLine;
	// vertex(xu, yu) can be calculated
	VectorLine sAxis, sDirtx;

	BoundedParabola(float focusX, float focusY,
		float aDirtx, float bDirtx, float cDirtx,
		float aBoundLine, float bBoundLine, float cBoundLine);

	BoundedParabola(float focusX, float focusY, 
		float aDirtx, float bDirtx, float cDirtx, 
		float aBoundLine, float bBoundLine, float cBoundLine, VectorLine sAxis);

	BoundedParabola(float* focus, VectorLine sAxis, float mpr, float npr);

	BoundedParabola();
	~BoundedParabola();

	float getPointval(float x, float y);
	float getDeterminantVal();

	bool isPointInsideParabola(float x, float y);
	bool isPointInside(float x, float y);

	float* getVertex(VectorLine sAxis, float a) {
		float u[] = { focusX - a *sAxis.dx, focusY - a *sAxis.dy };
		return u;
	}
};

