#include "AnswerSet.h"

AnswerSet::AnswerSet(GeoLocation location, int minDistToUpdate,
	VisibilityGraph* visGraph)
{
	this->location = GeoLocation(location);
	this->minDistToUpdate = minDistToUpdate;
	// this->poiSet = poiSet;
	// this->obsSet = obsSet;
	this->visGraph = visGraph;
}
AnswerSet::AnswerSet(GeoLocation location, int minDistToUpdate,
	vector<Obstacle*> poiSet, /*vector<Obstacle*> obsSet,*/ VisibilityGraph* visGraph)
{
	this->location = GeoLocation(location);
	this->minDistToUpdate = minDistToUpdate;
	this->poiSet = poiSet;
	//this->obsSet = obsSet;
	this->visGraph = visGraph;
}
AnswerSet::AnswerSet(GeoLocation location, vector<Obstacle*> poiSet,
	VisibilityGraph* visGraph,
	/*vector<Obstacle*> obsSet,*/ 
	vector<MyStruct> distO_q_pi)
{
	this->location = GeoLocation(location);
	this->minDistToUpdate = -1;
	this->poiSet = poiSet;
	//this->obsSet = obsSet;
	this->distO_vect = distO_q_pi;
	this->visGraph = visGraph;
}

AnswerSet::AnswerSet(GeoLocation location, 
	BoundedParabola piPoi, BoundedParabola piObst,
	vector<Obstacle*> poiSet,
	VisibilityGraph* visGraph,
	/*vector<Obstacle*> obsSet,*/
	vector<MyStruct> distO_q_pi)
{
	this->location = GeoLocation(location);
	this->minDistToUpdate = -1;
	this->poiParabola = piPoi;
	this->obsParabola = piObst;
	this->poiSet = poiSet;
	//this->obsSet = obsSet;
	this->distO_vect = distO_q_pi;
	this->visGraph = visGraph;
}

AnswerSet::~AnswerSet()
{
}
