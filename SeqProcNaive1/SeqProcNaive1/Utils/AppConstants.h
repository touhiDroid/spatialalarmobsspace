class AppConstants
{
public:
	enum { ALARM_PRIVATE, ALARM_SHARED, ALARM_PUBLIC };
	enum { MODE_BANDWIDTH_SAVING, MODE_COMPUTATIONAL_COST_SAVING };
	enum { NAIVE_ALGO, SAOS_ALGO};

	static const int MIN_ALTITUDE = 20;

	static const int ALGO_TYPE = SAOS_ALGO; // NAIVE_ALGO

	static const int ALGO_MODE = MODE_COMPUTATIONAL_COST_SAVING;
	// static const int ALARM_RADIUS = 70; // according to the normalized X-Y Space

	static const char* AppConstants::OBS_DATA_FILE_STR;
};