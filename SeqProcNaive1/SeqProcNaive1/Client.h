#pragma once
#include "GeoLocation.h"
#include "SAOSServer.h"
#include "visGraph/VisibilityGraph.h"
#include "visGraph/VisibilityGraphController.h"
#include <vector>
using namespace std;

class Client
{	
	void moveClient();
	void activateClient();

public:

	GeoLocation clientLocation;

	int clientId;
	float curM, curC, curVelocity;

	// vector< pair{slope(m), intercept(c)} >
	vector<pair<float, float>> pathHistoryVect; // -> y= mx + c => set of (m,c) pairs
												// m = m + random( -1.7321, 1.7321)  => m is allowed to be varied within -120 degrees to 120 degrees
												// c = y-mx, [current user location = (x,y)]

	Client(int clientId);
	Client(int clientId, GeoLocation curLocation);
	Client::Client(int clientId, float curM, float curC, float curVelocity,
		float curX, float curY);
	~Client();

	/*float* getClientLocation();
	void setClientLocation(GeoLocation newLoc);*/
};

