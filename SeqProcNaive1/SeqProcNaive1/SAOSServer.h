#pragma once
#include "GeoLocation.h"
#include "AnswerSet.h"
#include "VectorLine.h"
#include "Utils\AppConstants.h"

#include "blockfile/cache.h"
#include "rtree/rtree.h"
#include "rtree/rtnode.h"
#include "visGraph/obstacles.h"
#include "visGraph/obstacleController.h"
#include "visGraph/VisibilityGraph.h"
#include "visGraph/VisibilityGraphController.h"
#include "linlist/linlist.h"
#include "ognn/odist.h"

#include <fstream>
#include <vector>
using namespace std;

class SAOSServer
{
	char *TREEFILE = "../../DataFiles/sample.tree";// "../../DataFiles/sample_points.tree";
	char *TREEFILE_MBR = "../../DataFiles/sample_mbr.tree";
	const char *VIS_POLYGON_TXT_FILE_POI = "VisPolygonPoi.txt";
	const char *VIS_POLYGON_TXT_FILE_Obst = "VisPolygonObst.txt";

	const int blocksize = 2048;			//4096;//1024;//2048;
	const int b_length = 2048;
	const int dimension = 2;

	vector<Obstacle*> getAllPOIs(float *center, float radius);
	vector<Obstacle*> getAllObstacles(float *center, float radius);

	vector<Obstacle*> getPoisWithinParabola(BoundedParabola poiParabola);
	vector<Obstacle*> getObsWithinParabola(BoundedParabola obstParabola);

public:
	SAOSServer();
	~SAOSServer();

	SortedLinList *getAlarmables(GeoLocation q, float radius);
	AnswerSet *getAlarmables(float *q, float radius, AnswerSet aPrev);
	AnswerSet *queryAlarmables(float * q, float radius, VectorLine sAxis, 
		AnswerSet aPrev, 
		float mp, float np);

	AnswerSet * getKnownRegionData(float * q, float radius, VectorLine sAxis,
		AnswerSet aPrev, float mp, float np);

	float* getVertex(float * q, VectorLine sAxis, float a);
};

