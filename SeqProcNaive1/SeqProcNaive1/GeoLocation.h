#pragma once
class GeoLocation
{
public:
	float longitude, lattitude, altitude;

	GeoLocation(float longitude, float lattitude, float altitude);
	GeoLocation(float longitude, float lattitude);
	GeoLocation();
	~GeoLocation();
	
	char* getLocationStr();
};

