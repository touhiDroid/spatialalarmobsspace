/* entry.cpp
   implementation of class Entry */
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "entry.h"
#include "rtnode.h"
#include "../linlist/linlist.h"
//------------------------------------------------------------
Entry::Entry()
  //this ructor does nothing.  remember you should call init_entry
  //to initialize if you use this ructor
{
	son_ptr = NULL;
	bounces = NULL;
	nn_num=-1;
}
//------------------------------------------------------------
Entry::Entry(int _dimension, RTree *rt)
{
    dimension = _dimension;
    my_tree = rt;
    bounces = new float[2*dimension];
    son_ptr = NULL;
    son = 0;
	level = 0;
	nn_num=-1;
}
//------------------------------------------------------------
Entry::~Entry()
{
    if (bounces)
		delete [] bounces;
    if (son_ptr != NULL)
	    delete son_ptr;
}
//------------------------------------------------------------
void Entry::del_son()
{
	if (son_ptr != NULL)
	{
		delete son_ptr;
		son_ptr = NULL;
	}
}
//------------------------------------------------------------
Linkable* Entry::gen_Linkable()
{
	Linkable *new_link = new Linkable(dimension);
	new_link -> son = son;
	//memcpy(new_link -> bounces, bounces, 2 * dimension * sizeof(float));
	for (int i = 0; i < 2 * dimension; i ++)
		new_link -> bounces[i] = bounces[i];
	new_link -> level = level;

	return new_link;
}
//------------------------------------------------------------
int Entry::get_size()
{
    return 2 * dimension * sizeof(float) + sizeof(int);
	  //for bounces and son
}
//------------------------------------------------------------
RTNode* Entry::get_son()
{
    if (son_ptr == NULL)
	    son_ptr = new RTNode(my_tree, son);

    return son_ptr;
}
//------------------------------------------------------------
void Entry::init_entry(int _dimension, RTree *_rt)
{
	dimension = _dimension;
    my_tree = _rt;
    bounces = new float[2 * dimension];
    son_ptr = NULL;
    son = 0;
	level = 0;
}
//------------------------------------------------------------
void Entry::read_from_buffer(char *buffer)
{
    int i;

    i = 2 * dimension * sizeof(float);
    memcpy(bounces, buffer, i);

    memcpy(&son, &buffer[i], sizeof(int));
    i += sizeof(int);
}
//------------------------------------------------------------
SECTION Entry::section(float *mbr)
{
    bool inside;
    bool overlap;

    overlap = TRUE;
    inside = TRUE;

    for (int i = 0; i < dimension; i++)
    {
    	//printf("if (mbr[%d] > bounces[%d] ||  mbr[%d] < bounces[%d]) == %f>%f || %f>%f   (overlap=false)\n",2*i,2*i+1,2 * i + 1,2*i,mbr[2*i],bounces[2*i+1],mbr[2 * i + 1],bounces[2*i]);
		if (mbr[2 * i] > bounces[2 * i + 1] ||  mbr[2 * i + 1] < bounces[2 * i])
			overlap = FALSE;
		//printf("if (mbr[%d] > bounces[%d] ||  mbr[%d] < bounces[%d]) == %f>%f || %f>%f   (inside=false)\n",2*i,2*i,2 * i + 1,2*i+1,mbr[2*i],bounces[2*i],mbr[2 * i + 1],bounces[2*i+1]);
		if (mbr[2 * i] < bounces[2 * i] || mbr[2 * i + 1] > bounces[2 * i + 1])
			inside = FALSE;
    }
    if (inside)
		return INSIDE;
    else if (overlap)
		return OVERLAP;
    else
		return S_NONE;
}
//------------------------------------------------------------
bool Entry::section_circle(float *center, float radius)
{
	// float r2;

	// r2 = radius * radius;

	if ((radius - MINDIST(center,bounces,dimension)) < FLOATZERO)
		return TRUE;
	else
		return FALSE;
}
//------------------------------------------------------------
void Entry::set_from_Linkable(Linkable *link)
{
	son = link -> son;
	dimension = link -> dimension;
	memcpy(bounces, link -> bounces, 2 * dimension * sizeof(float));
	level = link -> level;

	my_tree = NULL;
	son_ptr = NULL;
}
//------------------------------------------------------------
void Entry::write_to_buffer(char *buffer)
{
    int i;

    i = 2 * dimension * sizeof(float);
    memcpy(buffer, bounces, i);

    memcpy(&buffer[i], &son, sizeof(int));
    i += sizeof(int);
}
//------------------------------------------------------------
bool Entry::operator == (Entry &_d)
  //this function compares two entries based on (1)son (2)dimension (3)extents
{
	if (son != _d.son) return false;
	if (dimension != _d.dimension) return false;
	for (int i = 0; i < 2 * dimension; i++)
		if (fabs(bounces[i] - _d.bounces[i]) > FLOATZERO) return false;
	return true;
}
/*bool Entry::section_parabola(float m, float n, float radius)
{
	float y2;

	y2 = bounces[2] * bounces[2];
	float a = m*radius;
	float nr = n*radius;
	if (((y2 - 4 * a*bounces[0]) < FLOATZERO) && (bounces[0]<nr))
		return true;
	else
		return false;
}*/
// Touhid
bool Entry::section_bounded_parabola(float focusX, float focusY,
	float aDirtx, float bDirtx, float cDirtx,
	float aBoundLine, float bBoundLine, float cBoundLine) {
	// Find whether on the same side of the bounding st. line as the focus
	float fVal = aBoundLine*focusX + bBoundLine*focusY + cBoundLine;
	float pVal = aBoundLine*bounces[0] + bBoundLine*bounces[2] + cBoundLine;
	if ((fVal > 0 && pVal < 0) || (fVal < 0 && pVal > 0) || pVal != 0)
		return false;

	// Find whether inside the parabola : g(x,y) = Ax^2 + Bxy + Cy^2 + Dx + Ey + F
	float A = bDirtx*bDirtx;
	float B = -2 * aDirtx*bDirtx;
	float C = aDirtx*aDirtx;
	float D = -2 * focusX - 2 * cDirtx*aDirtx;
	float E = -2 * focusY - 2 * bDirtx*cDirtx;
	float F = focusX*focusX + focusY*focusY - cDirtx*cDirtx;
	// Now, if sign( g(x1,y1) ) = sign( det( 2A B D | B 2C E | D E 2F ) )
	float x = bounces[0];
	float y = bounces[2];
	float gVal = A*x*x + B*x*y + C*y*y + D*x + E*y + F;
	float detVal = 2 * A* (4 * C*F - E*E) - B * (2 * B*F - E*D) + D * (B*E - 2 * C*D);
	if ((gVal < 0 && detVal < 0) || (gVal < 0 && detVal < 0) || gVal == 0)
		return true;

	return false;
}

// Touhid
bool Entry::section_bounded_parabola(BoundedParabola p) {
	// Find whether on the same side of the bounding st. line as the focus
	float fVal = p.aBoundLine*p.focusX + p.bBoundLine*p.focusY + p.cBoundLine;
	float pVal = p.aBoundLine*bounces[0] + p.bBoundLine*bounces[2] + p.cBoundLine;
	if ((fVal > 0 && pVal <= 0) || (fVal < 0 && pVal >= 0))// || pVal != 0)
		return false;
	float gVal = p.getPointval(bounces[0], bounces[2]);
	float detVal = p.getDeterminantVal();

	if ((gVal < 0 && detVal < 0) || (gVal < 0 && detVal < 0) || gVal == 0)
		return true;
	return false;
}
//------------------------------------------------------------
Entry& Entry::operator = (Entry &_d)
  //this function assigns all fieds of _d with the same values of this entry
{
    dimension = _d.dimension;
    son = _d.son;
    son_ptr = _d.son_ptr;
    memcpy(bounces, _d.bounces, sizeof(float) * 2 * dimension);
    my_tree = _d.my_tree;
	level = _d.level;

	nn_num=_d.nn_num;

    return *this;
}
//------------------------------------------------------------
