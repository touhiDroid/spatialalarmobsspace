#include "stdafx.h" 
#include <iostream> 
#include <fstream>
#include <string>
#include <algorithm>
#include<vector>
#include <time.h>

#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/linlist/linlist.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/blockfile/cache.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/rtree/rtree.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/rtree/rtnode.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/rtree/entry.h"

//#include "rsa.h"
//#include "IOkNN.h"

#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/visGraph/VisibilityGraph.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/visGraph/VisibilityGraphController.h"
#include "../SAOS_ExpImplementation/R_Tree_Usage_Code/visGraph/obstacleController.h"
using namespace std;

int io_access = 0;
int io_access_o = 0;
int io_access_p = 0;

const float MINX = 0;
const float MINY = 0;
const float MAXX = 10000;
const float MAXY = 10000;


char *DATAFILE_REAL = "Datasets/sample_points_real.txt";
char *DATAFILE_MBR_REAL = "Datasets/sample_mbr_real.txt";
char *DATAFILE_NORMALIZED = "Datasets/sample_points_normalized.txt";
char *DATAFILE_MBR_NORMALIZED = "Datasets/sample_mbr_normalized.txt";
char *DATAFILE_NORMALIZED_IFP = "Datasets/sample_points_normalized_ifp.txt";
char *DATAFILE_QUERY_POINTS = "Datasets/sample_query_points.txt";
char *TREEFILE = "Datasets/sample_points.tree"; // for POI
char *TREEFILE_MBR = "Datasets/sample_mbr.tree"; // for obstacles
struct obstacleMBR {
	float r[4];
};
class Stopwatch {
public:
	void start() {
		c1 = clock();
		t1 = time(0);
	}
	void stop() {
		c2 = clock();
		t2 = time(0);
	}
	int getDiff() {
		return (c2 - c1);
	}
private:
	time_t t1, t2;
	clock_t c1, c2;
};

void createDataPointFile()
{
	int temp;
	long id = 1;
	float x1, x2, y1, y2;

	char* dataPointFile = "Datasets/Germany/hypsogr_0/hypsogr.txt";

	ofstream outfile;
	outfile.open(DATAFILE_REAL);

	ifstream myfile;
	myfile.open(dataPointFile);
	srand(time(NULL));
	while (myfile >> temp >> x1 >> y1 >> x2 >> y2) {
		if ((temp % 11) == 1) {
			if (id != 1) {
				outfile << "\n";
			}
			if ((rand() % 2)) {
				outfile << id << " " << x1 << " " << x1 << " " << y1 << " " << y1;
			}
			else {
				outfile << id << " " << x2 << " " << x2 << " " << y2 << " " << y2;
			}
			id++;
		}
	}
	/*for(int i=0; i<1000; i++)
	{
	myfile >> temp >> x1 >> y1 >> x2 >> y2;
	outfile << id <<" "<< x1 <<" "<< x1 <<" "<< y1 <<" "<< y1<<"\n";
	id++;
	outfile << id <<" "<< x2 <<" "<< x2 <<" "<< y1 <<" "<< y1<<"\n";
	id++;
	outfile << id <<" "<< x2 <<" "<< x2 <<" "<< y2 <<" "<< y2<<"\n";
	id++;
	if(i!=999){
	outfile << id <<" "<< x1 <<" "<< x1 <<" "<< y2 <<" "<< y2<<"\n";
	}else{
	outfile << id <<" "<< x1 <<" "<< x1 <<" "<< y2 <<" "<< y2;
	}
	id++;
	}*/
	myfile.close();
	outfile.close();
}

void createMBRFile()
{
	int temp;
	long id = 1;
	float x1, x2, y1, y2;

	char* dataMBRFile = "Datasets/Germany/rrlines_0/rrlines.txt";

	ofstream outfile;
	outfile.open(DATAFILE_MBR_REAL);

	ifstream myfile;
	myfile.open(dataMBRFile);

	/*while(myfile >> id >> x1 >> y1 >> x2 >> y2)
	{
	if(id!=1) outfile<<"\n";
	outfile << id <<" "<< x1 <<" "<< x2 <<" "<< y1 <<" "<< y2;
	}*/

	srand(time(NULL));
	while (myfile >> temp >> x1 >> y1 >> x2 >> y2) {
		if ((temp % 15) == 1) {
			if (id != 1) outfile << "\n";
			outfile << id << " " << x1 << " " << x2 << " " << y1 << " " << y2;
			id++;
		}
	}
	/* for(int i=0; i<100; i++)
	{
	myfile >> id >> x1 >> y1 >> x2 >> y2;
	if(i!=99){
	outfile << id <<" "<< x1 <<" "<< x2 <<" "<< y1 <<" "<< y2<<"\n";
	}else{
	outfile << id <<" "<< x1 <<" "<< x2 <<" "<< y1 <<" "<< y2;
	}
	}*/
	myfile.close();
	outfile.close();
}

float val(float oldVal, float min, float max) {
	float newVal = (oldVal - min) / (max - min)*MAXX;
	return newVal;
}

//Normalizing real dataset into 0-10000
void normalizeMyRealDataset(const char *inputFile, const char *outputFile)
{
	ifstream input1;
	input1.open(inputFile);

	//Find the number of lines in the input file
	std::string line;
	int fileLength;
	for (fileLength = 0; std::getline(input1, line); ++fileLength)
		;
	input1.close();

	float r1[4];
	int x;
	float max = 0.0, min = 100000000;
	input1.open(inputFile);
	for (int i = 0; i<fileLength; i++)
	{
		input1 >> x >> r1[0] >> r1[1] >> r1[2] >> r1[3];

		if (r1[0]<min) min = r1[0]; if (r1[1]<min) min = r1[1]; if (r1[2]<min) min = r1[2]; if (r1[3]<min) min = r1[3];
		if (r1[0]>max) max = r1[0]; if (r1[1]>max) max = r1[1]; if (r1[2]>max) max = r1[2]; if (r1[3]>max) max = r1[3];
	}
	input1.close();

	ofstream outfile;
	outfile.open(outputFile);

	input1.open(inputFile);
	for (int i = 0; i<fileLength; i++)
	{
		input1 >> x >> r1[0] >> r1[1] >> r1[2] >> r1[3];
		if (i != fileLength - 1) {
			outfile << x << " " << val(r1[0], min, max) << " " << val(r1[1], min, max) << " " << val(r1[2], min, max) << " " << val(r1[3], min, max) << "\n";
		}
		else {
			outfile << x << " " << val(r1[0], min, max) << " " << val(r1[1], min, max) << " " << val(r1[2], min, max) << " " << val(r1[3], min, max);
		}
	}
	input1.close();
	outfile.close();
}

void generateDistinctPoints()
{
	int temp1, temp2;
	float x1, y1, x2, y2, x11, y11, x22, y22;

	ifstream inputFile1, inputFile2;
	ofstream outputfile;

	inputFile1.open(DATAFILE_NORMALIZED);

	//Find the number of lines in the input file
	std::string line;
	int fileLength;
	for (fileLength = 0; std::getline(inputFile1, line); ++fileLength)
		;
	inputFile1.close();

	outputfile.open("Datasets/sample_normalized_temp.txt");
	inputFile1.open(DATAFILE_NORMALIZED);
	int id = 1;
	for (int j = 0; j<fileLength; j++)
	{
		inputFile1 >> temp1 >> x1 >> x2 >> y1 >> y2;
		inputFile2.open(DATAFILE_NORMALIZED);
		int flag = 1;
		for (int i = 0; i<fileLength; i++)
		{
			inputFile2 >> temp2 >> x11 >> x22 >> y11 >> y22;
			if (temp1 != temp2) {
				if (x1 == x11 && y1 == y11) {
					cout << "Same for " << temp1 << " " << temp2 << endl;
					flag = 0;
					break;
				}
			}
		}
		if (flag) {
			if (id != 1) {
				outputfile << "\n";
			}
			outputfile << id << " " << x1 << " " << x2 << " " << y1 << " " << y2;
			id++;
		}
		inputFile2.close();
	}
	inputFile1.close();
	outputfile.close();

	remove(DATAFILE_NORMALIZED);
	rename("Datasets/sample_normalized_temp.txt", DATAFILE_NORMALIZED);
}

bool pointAtBoundary(float* point, float* mbr)
{
	bool atboundary = false;

	if (mbr[0] == point[0] && mbr[2] <= point[1] && mbr[3] >= point[1])
		atboundary = true;
	if (mbr[2] == point[1] && mbr[0] <= point[0] && mbr[1] >= point[0])
		atboundary = true;
	if (mbr[1] == point[0] && mbr[2] <= point[1] && mbr[3] >= point[1])
		atboundary = true;
	if (mbr[3] == point[1] && mbr[0] <= point[0] && mbr[1] >= point[0])
		atboundary = true;

	return atboundary;
}

bool intersects(float* mbr, float* bounces)
{
	bool inside;
	bool overlap;

	overlap = TRUE;
	inside = TRUE;

	for (int i = 0; i < 2; i++)
	{
		if (mbr[2 * i] > bounces[2 * i + 1] || mbr[2 * i + 1] < bounces[2 * i])
			overlap = FALSE;
		if (mbr[2 * i] < bounces[2 * i] || mbr[2 * i + 1] > bounces[2 * i + 1])
			inside = FALSE;
	}
	if (overlap || inside) {
		return true;
	}
	return false;
}

bool intersects2(float* mbr, float* bounces)
{
	bool inside;
	bool overlap;

	overlap = TRUE;
	inside = TRUE;

	for (int i = 0; i < 2; i++)
	{
		if (mbr[2 * i] >= bounces[2 * i + 1] || mbr[2 * i + 1] <= bounces[2 * i])
			overlap = FALSE;
		if (mbr[2 * i] < bounces[2 * i] || mbr[2 * i + 1] > bounces[2 * i + 1])
			inside = FALSE;
	}
	if (overlap || inside) {
		return true;
	}
	return false;
}

void generateIntersectFreePoints()
{
	float r1[4], r2[4], point[2];
	int fileLen1, fileLen2;
	ifstream input1, input2;
	ofstream outfile;

	//Find the number of lines in the input file
	std::string line;
	input1.open(DATAFILE_NORMALIZED);
	for (fileLen1 = 0; std::getline(input1, line); ++fileLen1)
		;
	input1.close();

	input2.open(DATAFILE_MBR_NORMALIZED);
	for (fileLen2 = 0; std::getline(input2, line); ++fileLen2)
		;
	input2.close();

	int x, y;
	input1.open(DATAFILE_NORMALIZED);
	outfile.open("Datasets/sample_points_normalized_ifp1.txt");
	int index = 1;
	for (int i = 0; i<fileLen1; i++)
	{
		input1 >> x >> r1[0] >> r1[1] >> r1[2] >> r1[3];
		input2.open(DATAFILE_MBR_NORMALIZED);
		bool noIntersect = true;
		for (int j = 0; j<fileLen2; j++)
		{
			input2 >> y >> r2[0] >> r2[1] >> r2[2] >> r2[3];
			if (intersects(r1, r2))
			{
				point[0] = r1[0]; point[1] = r1[2];
				if (!(r1[0] == r2[0] && r1[2] == r2[2] && r1[1] == r2[1] && r1[3] == r2[3])) {
					if (!pointAtBoundary(point, r2)) {
						noIntersect = false;
						break;
					}
					//else
					//	printf("\n at boundary\n");
					//fprintf(input3,"%d\t%d\t\t%f\t%f\t%f\t%f%s%d%s%f\t%f\t%f\t%f\n",x,y,r1[0],r1[2],r1[1],r1[3]," Intersects ",intersects(r1,r2),"  ",r2[0],r2[2],r2[1],r2[3]);
				}
			}
		}
		if (noIntersect) {
			outfile << index << " " << r1[0] << " " << r1[1] << " " << r1[2] << " " << r1[3] << "\n";
			index++;
		}
		input2.close();
	}
	input1.close();
	outfile.close();

	input1.open("Datasets/sample_points_normalized_ifp1.txt");
	outfile.open(DATAFILE_NORMALIZED_IFP);
	for (int i = 0; i<index - 1; i++)
	{
		input1 >> x >> r1[0] >> r1[1] >> r1[2] >> r1[3];
		if (i != index - 2) {
			outfile << i + 1 << " " << r1[0] << " " << r1[1] << " " << r1[2] << " " << r1[3] << "\n";
		}
		else {
			outfile << i + 1 << " " << r1[0] << " " << r1[1] << " " << r1[2] << " " << r1[3];
		}
	}
	input1.close();
	outfile.close();

	remove("Datasets/sample_points_normalized_ifp1.txt");
}

void generateIntersectFreeMBR()
{
	int temp1, temp2;
	float r1[4], r2[4];

	ifstream inputFile1, inputFile2;
	ofstream outputfile;

	inputFile1.open(DATAFILE_MBR_NORMALIZED);

	//find the number of lines in the input file
	std::string line;
	int filelength;
	for (filelength = 0; std::getline(inputFile1, line); ++filelength)
		;
	inputFile1.close();

	outputfile.open("Datasets/sample_mbr_normalized_temp.txt");
	inputFile1.open(DATAFILE_MBR_NORMALIZED);
	int id = 1;
	for (int j = 0; j<filelength; j++)
	{
		inputFile1 >> temp1 >> r1[0] >> r1[1] >> r1[2] >> r1[3];
		inputFile2.open(DATAFILE_MBR_NORMALIZED);
		int flag = 1;
		for (int i = 0; i<filelength; i++)
		{
			inputFile2 >> temp2 >> r2[0] >> r2[1] >> r2[2] >> r2[3];
			if (temp1 != temp2) {
				if (intersects2(r1, r2)) {
					cout << "same for " << temp1 << " " << temp2 << endl;
					flag = 0;
					break;
				}
			}
		}
		if (flag) {
			if (id != 1) {
				outputfile << "\n";
			}
			outputfile << id << " " << r1[0] << " " << r1[1] << " " << r1[2] << " " << r1[3];
			id++;
		}
		inputFile2.close();
	}
	inputFile1.close();
	outputfile.close();
	//cout<<"id "<<id<<endl;

	remove(DATAFILE_MBR_NORMALIZED);
	rename("Datasets/sample_mbr_normalized_temp.txt", DATAFILE_MBR_NORMALIZED);
}

void generate_nonIntersectingQueryPoints() {
	float defaultQArea = .005, r_x, r_y;
	float length = MAXX*defaultQArea, width = MAXY*defaultQArea;
	srand(10000);
	int count = 1;

	ofstream output;
	output.open(DATAFILE_QUERY_POINTS);
	while (count<121) {
		r_x = (float)rand() / RAND_MAX * 10000;
		r_y = (float)rand() / RAND_MAX * 10000;

		if ((r_x - length / 2) < 0 || (r_x + length / 2) > MAXX)
			continue;
		if ((r_y - width / 2) < 0 || (r_y + width / 2) > MAXY)
			continue;
		float r1[4], r2[4];
		r1[0] = r_x; r1[1] = r_x; r1[2] = r_y; r1[3] = r_y;
		bool intersect = false; int id;

		ifstream input1;
		input1.open(DATAFILE_MBR_NORMALIZED);
		while (input1 >> id >> r2[0] >> r2[1] >> r2[2] >> r2[3]) {
			if (intersects(r1, r2)) {
				intersect = true;
				break;
			}
		}
		input1.close();
		if (intersect)
			continue;
		else {
			cout << " x: " << r_x << " y: " << r_y << endl;
			if (count != 1) output << "\n";
			output << count << " " << r_x << " " << r_y;
			count++;
		}
	}
	output.close();
}


int main(int argc, char* argv[])
{

	//*****Creating Datasets****

	//createDataPointFile();
	//createMBRFile();
	//normalizeMyRealDataset(DATAFILE_REAL,DATAFILE_NORMALIZED);
	//normalizeMyRealDataset(DATAFILE_MBR_REAL,DATAFILE_MBR_NORMALIZED);
	//generateDistinctPoints();
	//generateIntersectFreeMBR();
	//generateIntersectFreePoints();
	//generate_nonIntersectingQueryPoints();

	//*****Create an RTree******

	int blocksize = 2048;			//4096;//1024;//2048;
	int b_length = 2048;
	int dimension = 2;

	//*****Point RTree********
	Cache *cache = new Cache(0, blocksize);

	//RTree *rt = new RTree(DATAFILE_NORMALIZED_IFP, TREEFILE, b_length, cache, dimension);
	RTree *rt = new RTree(TREEFILE, cache);

	//rt->print_tree();


	//******Obstacles RTree**********
	//Cache *cache_obs = new Cache(0, blocksize);

	// RTree *rt_obs = new RTree(DATAFILE_MBR_NORMALIZED, TREEFILE_MBR, b_length, cache_obs, dimension);
	//RTree *rt_obs = new RTree(TREEFILE_MBR, cache_obs);

	////******Range Query******
	float mbr[] = { 0, 8516.72, 0, 7908.62 };
	SortedLinList * result = new SortedLinList();
	SortedLinList * result1 = new SortedLinList();
	rt->rangeQuery(mbr, result);
	result->print();
	float center[] = { 8516.72,0,8516.72,7908.62 };
	float radius = 7909.72;
	rt->CircleQuery(center, radius, result1);
	result1->print();
	float m = 2;
	float n = 4;
	float r = 7909.72;
	rt->ParabolaQuery(m, n, r, result1);
	//result1->print();
	//rt_obs->print_tree();
	vector <Obstacle*> obslist;
	result->writeObstaclesInFile();
	string line;
	ifstream myfile("./Datasets/VisPolygon.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			Obstacle *temp = new Obstacle(line);
			obslist.push_back(temp);
		}
		myfile.close();
	}

	else cout << "Unable to open file";
	Obstacle* obs;
	obs = createObstacle("polygon((40 20,40 20))"); //For data point
	obslist.push_back(obs);

	obs = createObstacle("polygon((440 20,440 20))"); //For data point
	obslist.push_back(obs);

	obs = createObstacle("polygon((600 600,600 600))"); //For data point
	obslist.push_back(obs);
	VisibilityGraph* visGraph = new VisibilityGraph(obslist);
	VisibilityGraphController* vg = new VisibilityGraphController(visGraph);
	vector<Line*>  visEdges = vg->constructVisGraph();
	vector<Line*> obsSide = visGraph->obsSides;
	visGraph->print();
	getchar();
	delete rt;
	delete cache;

	//	delete rt_obs;
	//delete cache_obs;

	return 0;
}